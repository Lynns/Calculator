import java.awt.*;
import javax.swing.*;
import java.awt.event.*;
import java.util.*;


public class Calculator extends JFrame{
	
	// 菜单控件
	JMenuBar jmb;
	JMenu edit,view,help;
	JMenuItem edit_copy,edit_paste;
	JRadioButtonMenuItem view_hex,view_decimal,view_octal,view_binary;
	JMenuItem help_about;
	ButtonGroup rb_bg;
	
	// 编辑框
	JTextField jtf;
	JTextArea jta;
	
	// 面板
	JPanel jp1,jp2,jp3,jp4;
	
	// 主面板中部的南部
	JButton non_jb1,non_jb2,non_jb3;
	
	// 主面板中部面板的中部
	JButton[] arrbtn = new JButton[36];
	
	// 西部主面板
	JButton west_btn1,west_btn2,west_btn3,west_btn4,west_btn5;
	JButton btns;
	
	// 编辑文本
	String editText =""; 
	
	// 运算标志
	char opFlag = '\0';
	
	// 运算的数字
	double dLeft,dRight;
	
	// 运算符状态
	boolean bradixPoint = false;
	boolean bopFlag = false;
	
	// 复制数据
	String copyText = "";
	
	// 运算模式状态
	boolean bHex = false;
	boolean bOctal = false;
	boolean bBinary = false;
	boolean bDecimal = true;
	
	// 数据窗口
	JDialog dataDialog = new JDialog(Calculator.this,"数据域",true);
	JList dataList = new JList();
	JScrollPane jsp = new JScrollPane(dataList);
	Vector<String> data = new Vector<String>();
	JButton clear = new JButton("清空");
	
	public Calculator() {
		
		luachFrame();
		addButtonAction();
		
		dataDialog.add(jsp);
		dataDialog.add(clear,BorderLayout.SOUTH);
		dataDialog.setSize(200,200);
		dataDialog.setLocation(350-200,200);
		dataDialog.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
		clear.addActionListener(new ActionListener() {
			
			public void actionPerformed(ActionEvent arg0) {
				// TODO Auto-generated method stub
				if(data.size()!=0) {
					data.clear();
					btns.setText(""+data.size());
					dataList.setListData(data);
				}
			}
			
		});
		
		this.setTitle("计算器");
		this.setSize(450,320);
		this.setLocation(350,200);
		//this.setIconImage(new ImageIcon("image\\Calculator.png").getImage());
		this.setResizable(false);
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		this.setVisible(true);
	}
	public void luachFrame() {
		/**
		 * 菜单栏
		 * */
		jmb = new JMenuBar();
		edit = new JMenu("编辑(E)");
		edit.setMnemonic('E');
		edit.setFont(new Font("宋体",Font.PLAIN,12));
		view = new JMenu("查看(V)");
		view.setMnemonic('V');
		view.setFont(new Font("宋体",Font.PLAIN,12));
		help = new JMenu("帮助(H)");
		help.setMnemonic('H');
		help.setFont(new Font("宋体",Font.PLAIN,12));
		edit_copy = new JMenuItem("复制(C) Ctrl+C");
		edit_copy.setMnemonic('C');
		edit_copy.setFont(new Font("宋体",Font.PLAIN,12));
		edit_paste = new JMenuItem("粘贴(V) Ctrl+V");
		edit_paste.setMnemonic('V');
		edit_paste.setFont(new Font("宋体",Font.PLAIN,12));
		view_hex = new JRadioButtonMenuItem("十六进制(H)");
		view_hex.setMnemonic('H');
		view_hex.setFont(new Font("宋体",Font.PLAIN,12));
		view_decimal = new JRadioButtonMenuItem("十进制(D)");
		view_decimal.setMnemonic('D');
		view_decimal.setSelected(true);
		view_decimal.setFont(new Font("宋体",Font.PLAIN,12));
		view_octal = new JRadioButtonMenuItem("八进制(O)");
		view_octal.setMnemonic('O');
		view_octal.setFont(new Font("宋体",Font.PLAIN,12));
		view_binary = new JRadioButtonMenuItem("二进制(B)");
		view_binary.setMnemonic('B');
		view_binary.setFont(new Font("宋体",Font.PLAIN,12));
		help_about = new JMenuItem("关于计算器(A)");
		help_about.setMnemonic('A');
		help_about.setFont(new Font("宋体",Font.PLAIN,12));
		jmb.add(edit);
		jmb.add(view);
		jmb.add(help);
		edit.add(edit_copy);
		edit.add(edit_paste);
		view.add(view_hex);
		view.add(view_decimal);
		view.add(view_octal);
		view.add(view_binary);
		help.add(help_about);
		
		/**
		 * 编辑框
		 * */
		jtf = new JTextField();
		jtf.setEditable(false);
		jtf.setBackground(Color.white);
		jtf.setHorizontalAlignment(JTextField.RIGHT);
		jtf.setText("0.");
		jtf.setBorder(BorderFactory.createLoweredBevelBorder());
		
		/**
		 * 单选框(菜单项)
		 * */
		rb_bg = new ButtonGroup();
		rb_bg.add(view_hex);
		rb_bg.add(view_decimal);
		rb_bg.add(view_octal);
		rb_bg.add(view_binary);
		
		/**
		 * 面板西部
		 * */
		jp1 = new JPanel();
		jp1.setLayout(new GridLayout(6,1,5,5));
		west_btn1 = new JButton("MC");
		west_btn1.setFont(new Font("宋体",Font.PLAIN,12));
		west_btn1.setForeground(Color.RED);
		west_btn2 = new JButton("MR");
		west_btn2.setFont(new Font("宋体",Font.PLAIN,12));
		west_btn2.setForeground(Color.RED);
		west_btn3 = new JButton("MS");
		west_btn3.setFont(new Font("宋体",Font.PLAIN,12));
		west_btn3.setForeground(Color.RED);
		west_btn4 = new JButton("AVE");
		west_btn4.setFont(new Font("宋体",Font.PLAIN,12));
		west_btn5 = new JButton("SUM");
		west_btn5.setFont(new Font("宋体",Font.PLAIN,12));
		btns = new JButton();
		btns.setBackground(Color.WHITE);
		btns.setBorder(BorderFactory.createLoweredBevelBorder());
		btns.setPreferredSize(new Dimension(20,20));
		btns.setFont(new Font("宋体",Font.PLAIN,15));
		btns.setForeground(Color.BLUE);
		btns.setText("0");
		jp1.add(btns);
		jp1.add(west_btn1);
		jp1.add(west_btn2);
		jp1.add(west_btn3);
		jp1.add(west_btn4);
		jp1.add(west_btn5);
		
		/**
		 * 面板中部
		 * */
		non_jb1 = new JButton("BackSpace");
		non_jb1.setForeground(Color.RED);
		non_jb1.setFont(new Font("宋体",Font.PLAIN,12));
		non_jb2 = new JButton("CE");
		non_jb2.setForeground(Color.RED);
		non_jb2.setFont(new Font("宋体",Font.PLAIN,12));
		non_jb3 = new JButton("C");
		non_jb3.setForeground(Color.RED);
		non_jb3.setFont(new Font("宋体",Font.PLAIN,12));
		jp2 = new JPanel(new GridLayout(6,6,5,5));
		jp3 = new JPanel(new GridLayout(1,3,10,10));
		jp4 = new JPanel(new BorderLayout(5,5));
		jp3.add(non_jb1);
		jp3.add(non_jb2);
		jp3.add(non_jb3);
		
		arrbtn[0] = new JButton("1/x");
		arrbtn[0].setForeground(Color.RED);
		arrbtn[1] = new JButton("log");
		arrbtn[1].setForeground(Color.RED);
		arrbtn[2] = new JButton("7");
		arrbtn[2].setForeground(Color.BLUE);
		arrbtn[3] = new JButton("8");
		arrbtn[3].setForeground(Color.BLUE);
		arrbtn[4] = new JButton("9");
		arrbtn[4].setForeground(Color.BLUE);
		arrbtn[5] = new JButton("/");
		arrbtn[5].setForeground(Color.RED);
		
		arrbtn[6] = new JButton("n!");
		arrbtn[6].setForeground(Color.RED);
		arrbtn[7] = new JButton("sqrt");
		arrbtn[7].setForeground(Color.RED);
		arrbtn[8] = new JButton("4");
		arrbtn[8].setForeground(Color.BLUE);
		arrbtn[9] = new JButton("5");
		arrbtn[9].setForeground(Color.BLUE);
		arrbtn[10] = new JButton("6");
		arrbtn[10].setForeground(Color.BLUE);
		arrbtn[11] = new JButton("*");
		arrbtn[11].setForeground(Color.RED);
		
		arrbtn[12] = new JButton("sin");
		arrbtn[12].setForeground(Color.RED);
		arrbtn[13] = new JButton("x^2");
		arrbtn[13].setForeground(Color.RED);
		arrbtn[14] = new JButton("1");
		arrbtn[14].setForeground(Color.BLUE);
		arrbtn[15] = new JButton("2");
		arrbtn[15].setForeground(Color.BLUE);
		arrbtn[16] = new JButton("3");
		arrbtn[16].setForeground(Color.BLUE);
		arrbtn[17] = new JButton("-");
		arrbtn[17].setForeground(Color.RED);
		
		arrbtn[18] = new JButton("cos");
		arrbtn[18].setForeground(Color.RED);
		arrbtn[19] = new JButton("x^3");
		arrbtn[19].setForeground(Color.RED);
		arrbtn[20] = new JButton("0");
		arrbtn[20].setForeground(Color.BLUE);
		arrbtn[21] = new JButton("-/+");
		arrbtn[21].setForeground(Color.BLUE);
		arrbtn[22] = new JButton(".");
		arrbtn[22].setForeground(Color.RED);
		arrbtn[23] = new JButton("+");
		arrbtn[23].setForeground(Color.RED);
		
		arrbtn[24] = new JButton("tan");
		arrbtn[24].setForeground(Color.RED);
		arrbtn[25] = new JButton("x^y");
		arrbtn[25].setForeground(Color.RED);
		arrbtn[26] = new JButton("PI");
		arrbtn[26].setForeground(Color.CYAN);
		arrbtn[27] = new JButton("E");
		arrbtn[27].setForeground(Color.CYAN);
		arrbtn[28] = new JButton("%");
		arrbtn[28].setForeground(Color.RED);
		arrbtn[29] = new JButton("=");
		arrbtn[29].setForeground(Color.RED);
		
		arrbtn[30] = new JButton("A");
		arrbtn[31] = new JButton("B");
		arrbtn[32] = new JButton("C");
		arrbtn[33] = new JButton("D");
		arrbtn[34] = new JButton("E");
		arrbtn[35] = new JButton("F");
		
		for(int i=0;i<arrbtn.length;i++){
			arrbtn[i].setFont(new Font("宋体",Font.PLAIN,12));
			jp2.add(arrbtn[i]);
		}
		
		jp4.add(jp3,BorderLayout.NORTH);
		jp4.add(jp2,BorderLayout.CENTER);
		
		this.setJMenuBar(jmb);
		this.setLayout(new BorderLayout(10,5));
		this.add(jtf,BorderLayout.NORTH);
		this.add(jp1,BorderLayout.WEST);
		this.add(jp4,BorderLayout.CENTER);
		for(int i=30;i<=35;i++) {
			arrbtn[i].setEnabled(false);
		}
	}
	
	public void addButtonAction() {
		arrbtn[2].addActionListener(new NumAction());
		arrbtn[3].addActionListener(new NumAction());
		arrbtn[4].addActionListener(new NumAction());
		arrbtn[8].addActionListener(new NumAction());
		arrbtn[9].addActionListener(new NumAction());
		arrbtn[10].addActionListener(new NumAction());
		arrbtn[14].addActionListener(new NumAction());
		arrbtn[15].addActionListener(new NumAction());
		arrbtn[16].addActionListener(new NumAction());
		arrbtn[22].addActionListener(new NumAction());
		arrbtn[20].addActionListener(new NumAction());
		arrbtn[26].addActionListener(new NumAction());
		arrbtn[27].addActionListener(new NumAction());
		arrbtn[21].addActionListener(new NumAction());
		arrbtn[0].addActionListener(new NumAction());
		arrbtn[6].addActionListener(new NumAction());
		arrbtn[12].addActionListener(new NumAction());
		arrbtn[18].addActionListener(new NumAction());
		arrbtn[24].addActionListener(new NumAction());
		arrbtn[7].addActionListener(new NumAction());
		arrbtn[13].addActionListener(new NumAction());
		arrbtn[19].addActionListener(new NumAction());
		arrbtn[1].addActionListener(new NumAction());
		for(int i=30;i<=35;i++) {
			arrbtn[i].addActionListener(new NumAction());
		}
		
		arrbtn[17].addActionListener(new OpAction());
		arrbtn[11].addActionListener(new OpAction());
		arrbtn[5].addActionListener(new OpAction());
		arrbtn[28].addActionListener(new OpAction());
		arrbtn[23].addActionListener(new OpAction());
		arrbtn[25].addActionListener(new OpAction());
		
		arrbtn[29].addActionListener(new ResAction());
		
		non_jb1.addActionListener(new Clear());
		non_jb2.addActionListener(new Clear());
		non_jb3.addActionListener(new Clear());
		
		edit_copy.addActionListener(new EditItem());
		edit_paste.addActionListener(new EditItem());
		view_hex.addActionListener(new EditItem());
		view_decimal.addActionListener(new EditItem());
		view_octal.addActionListener(new EditItem());
		view_binary.addActionListener(new EditItem());
		help_about.addActionListener(new EditItem());
		
		btns.addActionListener(new DataDialog());
		west_btn1.addActionListener(new DataDialog());
		west_btn2.addActionListener(new DataDialog());
		west_btn3.addActionListener(new DataDialog());
		west_btn4.addActionListener(new DataDialog());
		west_btn5.addActionListener(new DataDialog());
	}
	
	private class NumAction implements ActionListener {

		public void actionPerformed(ActionEvent arg0) {
			
			try {
				if(arg0.getSource() == arrbtn[2]) {
					editText = editText + "7";
				}else if(arg0.getSource() == arrbtn[3]) {
					editText = editText + "8";
				}else if(arg0.getSource() == arrbtn[4]) {
					editText = editText + "9";
				}else if(arg0.getSource() == arrbtn[8]) {
					editText = editText + "4";
				}else if(arg0.getSource() == arrbtn[9]) {
					editText = editText + "5";
				}else if(arg0.getSource() == arrbtn[10]) {
					editText = editText + "6";
				}else if(arg0.getSource() == arrbtn[14]) {
					editText = editText + "1";
				}else if(arg0.getSource() == arrbtn[15]) {
					editText = editText + "2";
				}else if(arg0.getSource() == arrbtn[16]) {
					editText = editText + "3";
				}else if(arg0.getSource() == arrbtn[20]) {
					editText = editText + "0";
				}else if(arg0.getSource() == arrbtn[22]&&bradixPoint==false) {
					if(!editText.equals(""))
						editText = editText + ".";
					else
						editText = "0.";
					bradixPoint = true;
				}else if(arg0.getSource() == arrbtn[26]) {
					editText = String.format("%f",Math.PI);
				}else if(arg0.getSource() == arrbtn[27]) {
					editText = String.format("%f",Math.E);
				}else if(!editText.equals("")&&arg0.getSource() == arrbtn[21]) {
					double t = -Double.valueOf(editText);
					editText = Double.toString(t);
				}else if(!editText.equals("")&&arg0.getSource() == arrbtn[0]) {
					double t = 1.0/Double.valueOf(editText);
					editText = Double.toString(t);
				}else if(!editText.equals("")&&arg0.getSource() == arrbtn[12]) {
					double t = Math.sin(Double.valueOf(editText));
					editText = Double.toString(t);
				}else if(!editText.equals("")&&arg0.getSource() == arrbtn[18]) {
					double t = Math.cos(Double.valueOf(editText));
					editText = Double.toString(t);
				}else if(!editText.equals("")&&arg0.getSource() == arrbtn[24]) {
					double t = Math.tan(Double.valueOf(editText));
					editText = Double.toString(t);
				}else if(!editText.equals("")&&arg0.getSource() == arrbtn[7]) {
					double t = Math.sqrt(Double.valueOf(editText));
					editText = Double.toString(t);
				}else if(!editText.equals("")&&arg0.getSource() == arrbtn[13]) {
					double t = Double.valueOf(editText);
					editText = Double.toString(t*t);
				}else if(!editText.equals("")&&arg0.getSource() == arrbtn[19]) {
					double t = Double.valueOf(editText);
					editText = Double.toString(t*t*t);
				}else if(!editText.equals("")&&arg0.getSource() == arrbtn[6]) {
					double tn =Double.valueOf(editText);
					int n = (int)tn;
					int t = 1;
					for(int i=1;i<=n;i++)
						t*=i;
					editText = Double.toString(t);
				}else if(!editText.equals("")&&arg0.getSource() == arrbtn[1]) {
					double t = Math.log(Double.valueOf(editText));
					editText = Double.toString(t);
				}else if(arg0.getSource() == arrbtn[30]) {
					editText = editText + "A";
				}else if(arg0.getSource() == arrbtn[31]) {
					editText = editText + "B";
				}else if(arg0.getSource() == arrbtn[32]) {
					editText = editText + "C";
				}else if(arg0.getSource() == arrbtn[33]) {
					editText = editText + "D";
				}else if(arg0.getSource() == arrbtn[34]) {
					editText = editText + "E";
				}else if(arg0.getSource() == arrbtn[35]) {
					editText = editText + "F";
				}
				jtf.setText(editText);
			}catch(Exception e) {
				jtf.setText("转换错误，请按 C 键重启计算器!");
			}
		}
	}
	
	private class OpAction implements ActionListener {

		public void actionPerformed(ActionEvent arg0) {
			try {
				if(bopFlag == true) {
					return ;
				}
				if(editText.equals("")) {
					editText = "0.0";
				}
				if(arg0.getSource() == arrbtn[17]) {
					opFlag = '-';
				}else if(arg0.getSource() == arrbtn[23]) {
					opFlag = '+';
				}else if(arg0.getSource() == arrbtn[11]) {
					opFlag = '*';
				}else if(arg0.getSource() == arrbtn[5]) {
					opFlag = '/';
				}else if(arg0.getSource() == arrbtn[28]) {
					opFlag = '%';
				}else if(arg0.getSource() == arrbtn[25]) {
					opFlag = '^';
				}
				bopFlag = true;
				dLeft = Double.valueOf(editText);
				jtf.setText("0.");
				editText = "";
			}catch(Exception e) {
				jtf.setText("转换错误，请按 C 键重启计算器!");
			}
		}
		
	}
	
	private class ResAction implements ActionListener {
		
		public void actionPerformed (ActionEvent arg0) {
			try{
				String r="";
				if(opFlag == '\0') {
					return ;
				}else if(editText.equals("")&&(opFlag=='+'||opFlag=='-')){
					opFlag = '\0';
					r = String.format("%f",dLeft);
				}else if(editText.equals("")&&(opFlag=='*')){
					opFlag = '\0';
					r = String.format("%f",0.0);
				}else if(editText.equals("")&&(opFlag=='/')){
					opFlag = '\0';
					jtf.setText("除数不能为零");
					editText = "";
					dLeft = dRight = 0;
					return ;
				}else if(opFlag == '-') {
					opFlag = '\0';
					dRight = Double.valueOf(editText);
					r = String.format("%f",dLeft-dRight);
				}else if(opFlag == '+') {
					opFlag = '\0';
					dRight = Double.valueOf(editText);
					r = String.format("%f",dLeft+dRight);
				}else if(opFlag == '*') {
					opFlag = '\0';
					dRight = Double.valueOf(editText);
					r = String.format("%f",dLeft*dRight);
				}else if(opFlag == '/') {
					opFlag = '\0';
					dRight = Double.valueOf(editText);
					r = String.format("%f",dLeft/dRight);
				}else if(opFlag == '%') {
					opFlag = '\0';
					dRight = Double.valueOf(editText);
					r = String.format("%f",dLeft%dRight);
				}else if(opFlag == '^') {
					opFlag = '\0';
					dRight = Double.valueOf(editText);
					r = String.format("%f",Math.pow(dLeft,dRight));
				}
				int p=0;
				char[] cr = r.toCharArray();
				for(int i=cr.length-1;i>=0;i--) {
					if(cr[i]!='0') {
						p = i;
						break;
					}
				}
				r = r.substring(0,p+1);
				jtf.setText(r);
				editText = "";
				dLeft = dRight = 0;
				bradixPoint = bopFlag = false;
			}catch(Exception e) {
				jtf.setText("转换错误，请按 C 键重启计算器!");
			}
		}
	}
	
	private class Clear implements ActionListener {
		
		public void actionPerformed (ActionEvent arg0) {
			if(!editText.equals("")&&arg0.getSource() == non_jb1) {
				char[] ch = editText.toCharArray();
				ch[editText.length()-1] = '\0';
				editText = "";
				for(int i=0;ch[i]!='\0';i++) {
					editText = editText + ch[i];
				}
				if(editText.equals("")) {
					jtf.setText("0.");
				}else{
					jtf.setText(editText);
				}
			}else if(arg0.getSource() == non_jb2) {
				editText = "";
				jtf.setText("0.");
			}else if(arg0.getSource() == non_jb3) {
				dLeft = dRight = 0;
				bradixPoint = bopFlag = false;
				editText = "";
				opFlag = '\0';
				jtf.setText("0.");
			}
		}
	}
	
	private class EditItem implements ActionListener {

		public void actionPerformed(ActionEvent arg0) {
			// TODO Auto-generated method stub
			try {
				if(!editText.equals("")&&arg0.getSource() == edit_copy) {
					copyText = editText;
				}else if(arg0.getSource() == edit_paste) {
					jtf.setText(copyText);
				}else if(arg0.getSource() == view_hex) {
					bHex = true;
					for(int i=30;i<=35;i++)
						arrbtn[i].setEnabled(true);
					for(int i=0;i<=29;i++) {
							arrbtn[i].setEnabled(false);
					}
					for(int i=0;i<=29;i++) {
						if(i==2||i==8||i==9||i==10||i==14||i==15||i==16||i==3||i==4||i==20)
							arrbtn[i].setEnabled(true);
					}
					west_btn1.setEnabled(false);
					west_btn2.setEnabled(false);
					west_btn3.setEnabled(false);
					west_btn4.setEnabled(false);
					west_btn5.setEnabled(false);
					
					if(!editText.equals("")&&bDecimal==true) {
						long t = Long.valueOf(editText);
						editText = Long.toHexString(t);
						jtf.setText(editText);
						bDecimal = false;
					}else if(!editText.equals("")&&bOctal==true) {
						long t = Long.valueOf(OctToDec(editText));
						editText = Long.toHexString(t);
						jtf.setText(editText);
						bOctal = false;
					}else if(!editText.equals("")&&bBinary==true) {
						long t = Long.valueOf(BinToDec(editText));
						editText = Long.toHexString(t);
						jtf.setText(editText);
						bBinary = false;
					}
				}else if(arg0.getSource() == view_octal) {
					bOctal = true;
					for(int i=30;i<=35;i++)
						arrbtn[i].setEnabled(false);
					for(int i=0;i<=29;i++) {
							arrbtn[i].setEnabled(false);
					}
					for(int i=0;i<=29;i++) {
						if(i==2||i==8||i==9||i==10||i==14||i==15||i==16||i==20)
							arrbtn[i].setEnabled(true);
					}
					west_btn1.setEnabled(false);
					west_btn2.setEnabled(false);
					west_btn3.setEnabled(false);
					west_btn4.setEnabled(false);
					west_btn5.setEnabled(false);
					
					if(!editText.equals("")&&bDecimal==true) {
						long t = Long.valueOf(editText);
						editText = Long.toOctalString(t);
						jtf.setText(editText);
						bDecimal = false;
					}else if(!editText.equals("")&&bHex==true) {
						long t = Long.valueOf(HexToDec(editText));
						editText = Long.toOctalString(t);
						jtf.setText(editText);
						bHex = false;
					}else if(!editText.equals("")&&bBinary==true) {
						long t = Long.valueOf(BinToDec(editText));
						editText = Long.toOctalString(t);
						jtf.setText(editText);
						bBinary = false;
					}
				}else if(arg0.getSource() == view_decimal) {
					bDecimal = true;
					for(int i=30;i<=35;i++)
						arrbtn[i].setEnabled(false);
					for(int i=0;i<=29;i++) {
						arrbtn[i].setEnabled(true);
					}
					west_btn1.setEnabled(true);
					west_btn2.setEnabled(true);
					west_btn3.setEnabled(true);
					west_btn4.setEnabled(true);
					west_btn5.setEnabled(true);
					if(!editText.equals("")&&bHex==true) {
						editText = HexToDec(editText);
						jtf.setText(editText);
						bHex = false;
					}else if(!editText.equals("")&&bOctal==true) {
						editText = OctToDec(editText);
						jtf.setText(editText);
						bOctal = false;
					}else if(!editText.equals("")&&bBinary==true) {
						editText = BinToDec(editText);
						jtf.setText(editText);
						bBinary = false;
					}
				}else if(arg0.getSource() == view_binary) {
					bBinary = true;
					for(int i=30;i<=35;i++)
						arrbtn[i].setEnabled(false);
					for(int i=0;i<=29;i++) {
							arrbtn[i].setEnabled(false);
					}
					arrbtn[14].setEnabled(true);
					arrbtn[15].setEnabled(true);
					west_btn1.setEnabled(false);
					west_btn2.setEnabled(false);
					west_btn3.setEnabled(false);
					west_btn4.setEnabled(false);
					west_btn5.setEnabled(false);
					if(!editText.equals("")&&bDecimal==true) {
						long t = Long.valueOf(editText);
						editText = Long.toBinaryString(t);
						jtf.setText(editText);
						bDecimal = false;
					}else if(!editText.equals("")&&bHex==true) {
						long t = Long.valueOf(HexToDec(editText));
						editText = Long.toBinaryString(t);
						jtf.setText(editText);
						bHex = false;
					}else if(!editText.equals("")&&bOctal==true) {
						long t = Long.valueOf(OctToDec(editText));
						editText = Long.toBinaryString(t);
						jtf.setText(editText);
						bOctal = false;
					}
				}else if(arg0.getSource() == help_about) {
					String msg = "CRUD一把梭";
					JOptionPane.showConfirmDialog(Calculator.this,msg,"关于",JOptionPane.DEFAULT_OPTION);
				}
			}catch(Exception e) {
				jtf.setText("转换错误，请按 C 键重启计算器!");
			}
		}
		
	}
	
	private class DataDialog implements ActionListener {

		public void actionPerformed(ActionEvent arg0) {
			// TODO Auto-generated method stub
			if(arg0.getSource() == btns) {
				dataDialog.setVisible(true);
			}else if(data.size()!=0&&arg0.getSource() == west_btn1) {
				data.remove(data.size()-1);
				dataList.setListData(data);
				btns.setText(""+data.size());
			}else if(data.size()!=0&&arg0.getSource() == west_btn2) {
				editText = data.get(data.size()-1);
				jtf.setText(editText);
			}else if(!editText.equals("")&&arg0.getSource() == west_btn3) {
				data.add(editText);
				dataList.setListData(data);
				btns.setText(""+data.size());
				editText = "";
				jtf.setText(editText);
			}else if(data.size()!=0&&arg0.getSource() == west_btn4) {
				long t = 0;
				for(int i=0;i<data.size();i++){
					t += Long.valueOf(data.get(i));
				}
				editText = Double.toString(t/data.size());
				jtf.setText(editText);
			}else if(data.size()!=0&&arg0.getSource() == west_btn5) {
				long t = 0;
				for(int i=0;i<data.size();i++) {
					t += Long.valueOf(data.get(i));
				}
				editText = Long.toString(t);
				jtf.setText(editText);
			}
		}
		
	}
	
	private String HexToDec(String hex) {
		char[] ch = hex.toCharArray();
		int len = hex.length();
		long sum = 0;
		for(int i=0;i<len;i++) {
			int d;
			if(ch[i] == 'A' || ch[i] == 'a') d = 10;
			else if(ch[i] == 'B' || ch[i] == 'b') d = 11;
			else if(ch[i] == 'C' || ch[i] == 'c') d = 12;
			else if(ch[i] == 'D' || ch[i] == 'd') d = 13;
			else if(ch[i] == 'E' || ch[i] == 'e') d = 14;
			else if(ch[i] == 'F' || ch[i] == 'f') d = 15;
			else d = ch[i]-48;
			sum += d*Math.pow(16,len-1-i);
		}
		return Long.toString(sum);
	}
	
	private String OctToDec(String oct) {
		char[] ch = oct.toCharArray();
		int len = oct.length();
		long sum = 0;
		for(int i=0;i<len;i++) {
			sum += (ch[i]-48)*Math.pow(8,len-1-i);
		}
		return Long.toString(sum);
	}
	
	private String BinToDec(String bin) {
		char[] ch = bin.toCharArray();
		int len = bin.length();
		long sum = 0;
		for(int i=0;i<len;i++) {
			sum += (ch[i]-48)*Math.pow(2,len-1-i);
		}
		return Long.toString(sum);
	}
	
	public static void main(String[] args) {
		new Calculator();
	}
}
